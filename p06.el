;; Find out whether a list is a palindrome

(defun eq-head (a b) "Equality for the heads of two lists" ()
       (eq (car a) (car b)))

(defun emptyp (l) "Predicate empty list" ()
       (eq 0 (length l)))

(defun eq-list (a b) "Equality for two lists" ()
       (cond
        ((emptyp a) t)
        ((eq (car a) (car b)) (eq-list (cdr a) (cdr b)))
        (t nil)))

;; Urgh! After writing this I found out that there is a function for equality of
;; lists "equal".

(defun palindromep (l) "" ()
       (eq-list l (reverse l)))

