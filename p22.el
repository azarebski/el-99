
(defun range-increasing (a b)
  (if (= a b)
      (list b)
    (cons a (range-increasing (+ a 1) b))))

(defun range (a b)
  (if (<= a b)
      (range-increasing a b)
    (reverse (range-increasing b a))))


