(load "~/Documents/el-99/p10.el")
(load "~/Documents/el-99/p35.el")

(defun prime-factors-mult (n)
  (mapcar 'reverse (encode (prime-factors n))))
