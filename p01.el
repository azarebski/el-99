;; Find the element of a list.
;;
;; The recursive definition comes from the observation that the last element of
;; a list is the same as the last element of the tail of a list.

(defun my-last (l)
  (if (null l)
      nil
    (if (null (rest l))
        (car l)
      (my-last (rest l))
      )
    )
  )
