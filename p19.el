(load "~/Documents/el-99/p17.el")

(defun rotate (xs n)
  (if (zerop n)
      xs
    (if (> n 0)
        (apply 'append (reverse (split xs n)))
      (apply 'append (mapcar 'reverse (split (reverse xs) (- 0 n)))
        ))
  ))
