(load "~/Documents/el-99/p55.el")       ; cbbt :: Int -> [Tree]
(load "~/Documents/el-99/p56.el")       ; symmetric-top-p :: Tree -> Bool
(load "dash")                           ; -filter :: (x -> Bool) -> [x] -> [x]

(defun sym-cbbt (n)
  "Return a list of the symmetric completely balanced binary trees."
  (-filter 'symmetric-top-p (cbbt n)))
