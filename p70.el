;; Parser to construct the tree from a string.
;;
(load "parsec")

(defun mt-node ()
  (make-symbol (parsec-letter)))

(defun mt-caret ()
  (parsec-ch ?^))

(defun mt-up ()
  (if (equal (mt-caret) "^")
      '[]))

(defun mt-mtree ()
  (list (mt-node) (mt-forest)))

(defun mt-forest ()
  (apply 'vector (parsec-endby (mt-mtree) (mt-caret))))

(defun read-mtree (str-mt)
  (parsec-with-input str-mt (mt-mtree)))


;; Shower to encode the tree in a string.
;;

(defun intersperse (xs y)
  (if (null xs)
      xs
      (cons (first xs) (cons y (intersperse (rest xs) y)))))

(defun show-forest (f)
  (if (= 0 (length f))
      ""
    (apply 'concat (intersperse (mapcar 'show-mtree-rec f) "^"))))

(defun show-mtree-rec (mt)
  (let ((root-str (symbol-name (first mt)))
        (forest-str (show-forest (second mt))))
    (concat root-str forest-str)))

(defun show-mtree (mt)
  (concat (show-mtree-rec mt) "^"))
