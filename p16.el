(load "dash")

(defun zip-ix (xs) (-zip (number-sequence 1 (length xs)) xs))

(defun pred-ix (p n) (not (zerop (% (car p) n))))

;; Drop every n'th element from a list.

(defun drop (xs n)
  (-map 'cdr (-filter (lambda (p) (pred-ix p n)) (zip-ix xs)))
  )
