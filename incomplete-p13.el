(load "dash")

(defun foo (n x ys) "" ()
       (if (null ys)
           (if (zerop n)
               nil
             (if (eql n 1)
                 x
               (list n x)))
           ;; (list (list n x) ys)
         (if (zerop n)
           (foo 1 (car ys) (cdr ys))
           (if (eql (car ys) x)
               (foo (+ n 1) x (cdr ys))
             (if (eql n 1)
                 (list x ys)
               (list (list n x) ys))))))

(defun foo-wrapper (xs) "" ()
       (if (listp xs)
           (foo 0 nil xs)
         xs)
       )

(defun digest (f xs) "" ()
       (if (null xs)
           nil
         (let ((ab (apply f '(xs))))
           (append (car ab) (digest f (car (cdr xs)))))))

(defun demo (l) "" ()
       (list 1 (cdr l)))
