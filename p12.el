(load "~/Documents/el-99/p07")          ; provides: my-flatten
(load "~/Documents/el-99/p09")          ; provides: fst, snd


(defun decoder (l) "" ()
       (if (listp l)
       (let ((n (fst l)) (x (snd l)))
         (make-list n x)
         )
       l))


(defun decode-modified (l) "" ()
       (my-flatten (mapcar 'decoder l))
       )
