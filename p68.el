
(load "dash")


(defun preorder-nodes (tree)
  "Return the nodes in pre-order."
  (cond
   ((listp tree) (-flatten (-map 'preorder-nodes tree)))
   ((atom tree) (symbol-name tree))))

(defun inorder-nodes (tree)
  "Return the nodes in in-order."
  (cond
   ((null tree) tree)
   ((listp tree) (-flatten (-map 'inorder-nodes (list (second tree) (first tree) (third tree)))))
   ((atom tree) (symbol-name tree))
   ))

;; Next step is to reconstruct the tree from the nodes...

(defvar demo-tree '(f (b (a nil nil) (d (c nil nil) (e nil nil))) (g nil (i (h nil nil) nil))))

;; 1. The first node of the preorder is the root of the tree.

(defvar demo-pon '("f" "b" "a" "d" "c" "e" "g" "i" "h"))
(defvar demo-ion '("a" "b" "c" "d" "e" "f" "g" "h" "i"))

(defun foo (preorder-nodes inorder-nodes)
  (if (null preorder-nodes)
      (progn
        nil)
    (let* ((root (car preorder-nodes))
         (left-inorder-nodes (-take-while (lambda (x) (not (equal x root))) inorder-nodes))
         (right-inorder-nodes (cdr (-drop-while (lambda (x) (not (equal x root))) inorder-nodes)))
         (left-preorder-nodes (-take-while (lambda (x) (member x left-inorder-nodes)) (cdr preorder-nodes)))
         (right-preorder-nodes (-drop-while (lambda (x) (not (member x right-inorder-nodes))) (cdr preorder-nodes)))
         (left-tree (foo left-preorder-nodes left-inorder-nodes))
         (right-tree (foo right-preorder-nodes right-inorder-nodes))
         )
    (list (make-symbol root) left-tree right-tree))))
