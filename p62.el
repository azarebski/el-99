
(defun internals (tree)
  "Return a list of the internal nodes of a binary tree."
  (if (atom tree)
      nil
    (cons (first tree) (append (internals (second tree)) (internals (third tree))))))
