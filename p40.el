(load "~/Documents/el-99/p39.el")
(load "cl")

(defun zip1 (x ys)
  (mapcar (lambda (y) (list x y)) ys))

(defun cartesian-rec (xs ys zs)
  (if (null ys)
      nil
    (if (null xs)
        zs
      (let ((x (first xs))
            (tail (rest xs)))
        (cartesian-rec tail ys (append (zip1 x ys) zs))))))

(defun cartesian (xs ys)
  (cartesian-rec xs ys nil))

(defun take-first (p xs)
  (if (null xs)
      nil
    (if (apply p (list (first xs)))
        (first xs)
      (take-first p (rest xs)))))

(defun goldbach (n)
  (setq primes (sieve n))
  (setq prime-pairs (cartesian primes primes))
  (setq sum-to-n (lambda (x) (= n (apply '+ x))))
  (take-first sum-to-n prime-pairs)
  )
