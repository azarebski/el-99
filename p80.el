(load "dash")

;; graph-term form
;; graph([b,c,d,f,g,h,k],[e(b,c),e(b,f),e(c,f),e(f,k),e(g,h)])

;; adjacency-list form
;; [n(b,[c,f]), n(c,[b,f]), n(d,[]), n(f,[b,c,k]), ...]

;; The functions =gt-to-al= and =al-to-gt= transform between the different forms
;; of the graphs. There are demonstration graphs to test this with.

(setq demo-graph-term '((nodes . (b c d f g h k))
                        (edges . ((b c) (b f) (c f) (f k) (g h)))))

(setq demo-adj-list '((b (c f))
                      (c (b f))
                      (d nil)
                      (f (b c k))
                      (g (h))
                      (h (g))
                      (k (f))))

(defun gt-nodes (gtg)
  "Return the nodes from a graph-term graph."
  (cdr (assoc 'nodes gtg)))

(defun gt-edges (gtg)
  "Return the edges from an adjacency-list graph."
  (cdr (assoc 'edges gtg)))

(defun al-nodes (alg)
  "Return the nodes from a graph-term graph."
  (-map 'car alg))

(defun order-edge (edge)
  "Sorts the order of nodes in an edge."
  (-map 'read (-sort 'string< (-map 'symbol-name edge))))

(defun al-edges-helper (adjs)
  ""
  (let ((in-node (first adjs))
        (out-nodes (second adjs)))
    (if (null out-nodes)
        nil
      (-map (lambda (x) (order-edge (list in-node x))) out-nodes))))

(defun al-edges (alg)
  "Return the edges from an adjacency-list graph."
  (-distinct (-flatten-n 1 (-map 'al-edges-helper alg))))

(defun al-to-gt (alg)
  "Return a graph-term form for the given adjacency-list form."
  (let ((nodes (al-nodes alg))
        (edges (al-edges alg)))
    (list (cons 'nodes nodes) (cons 'edges edges))))

(defun neighbours (node edges)
  (list node (-filter (lambda (nn) (not (equal node nn))) (-flatten (-filter (lambda (edge) (member node edge)) edges)))))

(defun gt-to-al (gtg)
  "Return an adjacency-list form for the given graph-term form."
  (let ((nodes (gt-nodes gtg))
        (edges (gt-edges gtg)))
    (-map (lambda (n) (neighbours n edges)) nodes)))
