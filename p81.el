(load "dash")

;; adjacency-list form
;; [n(k,[]),n(m,[q/7]),n(p,[m/5,q/9]),n(q,[])]

(setq demo-al-graph '((b [c f]) (c [b f]) (d []) (f [b c k]) (g [h]) (h [g]) (k [f])))

(defun al-neighbours (alg x)
  (seq-into (second (-flatten (-filter (lambda (y) (member x y)) alg))) 'list))

;; Partial solution which will return a single path from a to b if it exists and
;; will return nil if one of length not greater than n does not exist.
(defun al-path (alg a b n)
  (if (eq n 0)
      nil
  (if (eq a b)
      (list a)
    (let* ((neighbours (al-neighbours alg a))
           (maybe-path (al-path alg (first neighbours) b (- n 1))))
      (cond ((null maybe-path) nil)
            ((member b maybe-path) (cons a maybe-path))
            ((null (cdr neighbours)) nil)
            (t (al-path alg (second neighbours) b (- n 1))))))))
