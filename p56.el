(defun eql-top (t1 t2)
  "Equality check for the topology of two binary trees."
  (if (null t1)
      (null t2)
    (if (null t2)
        nil
      (let ((t1-left (second t1))
            (t1-right (third t1))
            (t2-left (second t2))
            (t2-right (third t2)))
        (and (eql-top t1-left t2-left) (eql-top t1-right t2-right))))))

(defun rev-tree (tree)
  "Return the mirror image of the tree."
  (if (null tree)
      tree
    (let ((root (first tree))
          (left-tree (second tree))
          (right-tree (third tree)))
      (list root right-tree left-tree))))

(defun symmetric-top-p (tree)
  (if (null tree)
      t
    (let ((left-tree (second tree))
          (right-tree (third tree)))
      (eql-top left-tree (rev-tree right-tree)))))

;; (setq demo-t1 '(x (x nil nil) nil))
;; (setq demo-t2 '(x (x nil nil) (x nil nil)))
;; (setq demo-t3 '(x nil (x nil nil)))
