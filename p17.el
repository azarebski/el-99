(defun prepend (xs x)
  (reverse (cons x (reverse xs))))


(defun foo (a b n)
  (if (zerop n)
      (list a b)
    (if (null b)
        (list a b)
      (let ((new-a (prepend a (car b))) (new-b (cdr b)) (new-n (- n 1)))
        (foo new-a new-b new-n)))))

(defun split (xs n)
  (foo nil xs n))
