(load "~/Documents/el-99/p17.el")


(defun insert-at (x xs n)
  (let ((sxs (split xs (- n 1))))
    (append (car sxs) (cons x (car (cdr sxs))))))
