
(defun tree-size (tree)
  "Return the number of nodes in a tree."
  (if (null tree)
      0
    (+ 1 (tree-size (second tree)) (tree-size (third tree)))))

(defun layout-rec (tree left-pad depth)
  "Recursive function to find the node placement."
  (if (null tree)
      nil
    (let ((val (first tree))
          (x-coord (+ left-pad (tree-size (second tree))))
          (y-coord depth)
          (left-coords (layout-rec (second tree) left-pad (+ 1 depth)))
          (right-coords (layout-rec (third tree) (+ 1 left-pad (tree-size (second tree))) (+ 1 depth))))
      (cons (list val x-coord y-coord) (append left-coords right-coords)))))

(defun layout (tree)
  "Return a list of (value x y) coordinates."
  (layout-rec tree 1 1))

(setq demo-tree '(n (k (c (a nil nil) (h (g (e nil nil) nil) nil)) (m nil nil)) (u (p nil (s (q nil nil) nil)) nil)))
