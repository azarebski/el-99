(defun cbt-rec (n a)
  "Return the complete binary tree on n nodes."
  (if (< n a)
      nil
    (if (= n a)
        (list a nil nil)
      (if (= n (* 2 a))
          (list a (cbt-rec n (* 2 a)) nil)
        (list a (cbt-rec n (* 2 a)) (cbt-rec n (+ 1 (* 2 a))))))))

(defun cbt (n)
  "Return the complete binary tree on n nodes."
  (cbt-rec n 1))
