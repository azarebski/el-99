(load "~/Documents/el-99/p55.el")       ; cross-trees :: [Trees] -> [Trees] -> [Trees]
(load "dash")

;; tree-height :: Tree -> Int
;; is-leaf-p :: Tree -> Boolean


(defun is-leaf-p (tree)
  "Predicate for whether a tree is just a leaf."
  (null tree))

(defun tree-height (tree)
  "Return the height of a tree."
  (if (is-leaf-p tree)
      0
      (+ 1 (-max (-map 'tree-height (rest tree))))))

(defun hbbt (depth)
  "Return a list of all the height balanced binary trees of depth height."
  (if (= depth 0)
      '(nil)
    (if (= depth 1)
        '((x nil nil))
      (let ((short-subs (hbbt (- depth 2)))
            (long-subs (hbbt (- depth 1))))
        (append (cross-trees short-subs long-subs) (cross-trees long-subs short-subs))))))
