(load "parsec")

(defun t-node ()
  (make-symbol (parsec-or (parsec-letter) "nil")))

(defun t-lb ()
  (parsec-ch ?())

(defun t-rb ()
  (parsec-ch ?)))

(defun t-comma ()
  (parsec-ch ?,))

(defun t-lstree ()
  (parsec-return (parsec-and (t-lb) (t-tree)) (t-comma)))

(defun t-rstree ()
  (parsec-return (t-tree) (t-rb)))

(defun t-tree ()
  (parsec-or
   (parsec-try (list (t-node) (t-lstree) (t-rstree)))
   (t-node)))
