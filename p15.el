(load "dash")

(defun repli (l n)
  (apply '-concat (-map (lambda (x) (-repeat n x)) l)))
