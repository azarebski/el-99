(defun my-flatten (l) "" ()
       (if (eql l nil)
           nil                          ; the empty list is already flat
         (let ((head (car l)) (tail (cdr l)))
           (if (listp head)
               (append (my-flatten head) (my-flatten tail))
               (append (list head) (my-flatten tail)) ; need to make the head a list to append it to the flattened tail.
             )
           )
         )
       )
