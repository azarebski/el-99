

(load "~/Documents/el-99/p31.el")

(defun dividesp (p n)
  "Predicate for p divides n."
  (zerop (mod n p)))

;; (defun leq-primes (n)
;;   "A list of the primes which are not larger than n."
;;   (cond
;;    ((= n 2) (list 2))
;;    ((= n 3) (list 3 2))
;;    ((evenp n) (leq-primes (- n 1)))
;;    ((is-prime n) (cons n (leq-primes (- n 2))))
;;    (t (leq-primes (- n 2))))
;;   )

(defun prime-factors-rec (n p-cand ps)
  (if (> p-cand n)
      ps
    (if (is-prime p-cand)
      (if (dividesp p-cand n)
          (cons p-cand (prime-factors-rec (/ n p-cand) p-cand ps))
        (prime-factors-rec n (+ p-cand 1) ps))
    (prime-factors-rec n (+ p-cand 1) ps)))
  )


(defun prime-factors (n)
  (prime-factors-rec n 2 nil))
