(defun num-leaves-binary-tree (tree)
  "Return the number of leaves in a binary tree."
  (if (null tree)
      1
    (+ (num-leaves-binary-tree (second tree)) (num-leaves-binary-tree (third tree)))))
