
(defun at-level (tree level)
  (if (null tree)
      (list tree)
    (if (= level 1)
        (if (or (atom tree) (null tree))
            (list tree)
          (list (first tree)))
      (if (atom tree)
          nil
        (append (at-level (second tree) (- level 1)) (at-level (third tree) (- level 1))))))
  )
