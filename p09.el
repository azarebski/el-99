;; Pack consecutive duplicates of list elements into sublists.

(defun foo (a b) "" ()
       (if (null b)
           (cons a (list b))
         (let ((head (car b)) (tail (cdr b)))
           (if (or (member head a) (null a))
               (foo (cons head a) tail)
             (cons a (list b))
           )
         )
         )
       )

(defun fst (l) "" () (car l))
(defun snd (l) "" () (car (cdr l)))

(defun pack-helper (b c) "" ()
       (if (null b)
           (reverse c)
         (let ((bar (foo () b)))
           (pack-helper (snd bar) (cons (fst bar) c)))))

(defun pack (l) "" ()
       (pack-helper l ()))
