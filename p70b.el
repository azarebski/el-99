(defun treep (x)
  (if (symbolp x)
      nil
    (if (= (length x) 2)
      (let ((root (first x))
            (forest (second x)))
        (if (symbolp root)
            (if (sequencep forest)
                (if (null-seq forest)
                    t
                  (seq-every-p 'treep forest))
              nil)
          nil))
      nil)))

(defun null-seq (s)
  (= (length s) 0))


(defvar demo-mtree '(a [(f [(g [])]) (c []) (b [(d []) (e [])])]))
