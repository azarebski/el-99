
(defun leaves (tree)
  "Return the leaves of a binary tree."
  (if (atom tree)
      (list tree)
    (append (leaves (second tree)) (leaves (third tree)))))
