(load "~/Documents/el-99/p70.el")

(defun leaf-p (mt)
  (= (length (second mt)) 0))


(defun ipl-rec (mt n)
  (if (leaf-p mt)
      (+ n 0)
    (let ((forest (second mt))
          (depth (lambda (st) (ipl st (+ n 1)))))
      (apply '+ (cons n (mapcar (lambda (st) (ipl-rec st (+ n 1))) forest)))
      )))


(defun ipl (mt)
  (ipl-rec mt 0))
