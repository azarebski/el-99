
(defun p-list-rec (p n xs)
  (let ((x (first xs)))
    (if (> x n)
        (rest xs)
      (p-list-rec p n (cons (+ x p) xs)))))

(defun p-list (p n)
  (p-list-rec p n (list (* 2 p))))

(defun sieve-rec (p n marked primes)
  (if (> p n)
      primes
    (if (member p marked)
        (sieve-rec (+ p 1) n marked primes)
      (sieve-rec (+ p 1) n (append (p-list p n) marked) (cons p primes)))))

(defun sieve (n)
  (reverse (sieve-rec 2 n nil nil)))
