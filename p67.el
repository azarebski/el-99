(load "dash")


(defun tree-as-string (tree)
  (if (null tree)
      ""
    (if (atom tree)
        (symbol-name tree)
      (let ((node-name (symbol-name (first tree)))
            (left-string (tree-as-string (second tree)))
            (right-string (tree-as-string (third tree))))
        (if (and (eq "" left-string) (eq "" right-string))
            node-name
          (format "%s(%s,%s)" node-name left-string right-string))))))




(setq demo-tree '(a (b (d nil nil) (e nil nil)) (c nil (f (g nil nil) nil))))



(defun eval-string (string)
  (eval (car (read-from-string string))))

(defun string-as-chars (string)
  (reverse (cdr (reverse (cdr (split-string string ""))))))

(defun char-pairs (string)
  (let* ((chars (string-as-chars string))
         (head-chars (reverse (cdr (reverse chars))))
         (tail-chars (cdr chars)))
    (-zip head-chars tail-chars)))

(defun alpha-p (char)
  (string-match "[[:alpha:]]" char))

(defun comma-p (char)
  (string-match "," char))

(defun lb-p (char)
  (string-match "(" char))

(defun rb-p (char)
  (string-match ")" char))

(defun replacements (pair)
  (let ((a (car pair))
        (b (cdr pair)))
    (cond
     ((and (alpha-p a) (lb-p b)) (format "(%s " a))
     ((and (alpha-p a) (comma-p b)) (format "(%s nil nil) " a))
     ((and (alpha-p a) (rb-p b)) (format "(%s nil nil))" a))
     ((and (rb-p a) (rb-p b) "))"))
     ((and (lb-p a) (comma-p b)) " nil ")
     ((and (comma-p a) (rb-p b)) " nil ")
     ((and (comma-p a) (alpha-p b)) " ")
     (t "")
     )))

(defun parse-to-expression (string)
  (car (read-from-string (apply 'concat (cons "'" (-map 'replacements (char-pairs string)))))))
