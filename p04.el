;; Find the number of elements of a list.

(defun my-length (xs)
  (if (null xs)
      0
    (+ 1 (my-length (rest xs)))
    )
  )
