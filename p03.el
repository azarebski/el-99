;; Fine the k'th element of a list.

(defun element-at (xs n) "" ()
       (if (= 1 n)
           (first xs)
         (element-at (rest xs) (- n 1))
         )
       )
