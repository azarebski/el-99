(load "dash")


(defun cross-trees (left-trees right-trees)
  "Return a list of trees in bijection to the cartesian product of the lists supplied."
  (-table-flat (lambda (lt rt) (list 'x lt rt)) left-trees right-trees))

(defun cbbt (n)
  "Return a list of all the completely balanced binary trees on n nodes."
  (if (= n 0)
      (list nil)
    (if (= n 1)
        (list (list 'x nil nil))
      (if (oddp n)
        (let ((sub-trees (cbbt (/ (- n 1) 2))))
          (cross-trees sub-trees sub-trees))
      (let ((small-subs (cbbt (/ (- n 1) 2)))
            (large-subs (cbbt (+ 1 (/ (- n 1) 2)))))
        (append (cross-trees small-subs large-subs) (cross-trees large-subs small-subs)))))))
