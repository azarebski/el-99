
(load "~/Documents/el-99/p33.el")       ; coprime :: Int -> Int -> Bool

(defun totient-phi-helper (m x)
  (if (= x 1)
      1
    (let ((sub-result (totient-phi-helper m (- x 1))))
      (cond
       ((= x m) sub-result)
       ((coprime m x) (+ 1 sub-result))
       (t sub-result)))))

;; (defun totient-phi-helper (m x)
;;   (if (= x 1)
;;       1
;;     (if (= x m)
;;         (totient-phi-helper m (- x 1))
;;       (if (coprime m x)
;;           (+ 1 (totient-phi-helper m (- x 1)))
;;         (totient-phi-helper m (- x 1))))))

(defun totient-phi (m)
  (totient-phi-helper m m))
