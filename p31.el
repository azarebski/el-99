
(defun potential-divs-helper (xs n)
  (let ((max-x (apply 'max xs)))
    (if (>= max-x n)
        xs
      (potential-divs-helper (cons (+ 2 max-x) xs) n))))

(defun potential-divisors (n)
  (let ((k (ceiling (sqrt n))))
    (cond
     ((> 4 n) nil)
     ((> 6 n) (list 2))
     (t (potential-divs-helper (list 3 2) k)))))

(defun divisor-in-list (n xs)
  (if (null xs)
      nil
    (let ((head (first xs))
          (tail (rest xs)))
      (if (= 0 (mod n head))
          t
        (divisor-in-list n tail)))))

(defun is-prime (n)
  (if (< n 2)
      nil
    (let ((pdivs (potential-divisors n)))
    (not (divisor-in-list n pdivs)))))

(defun tester-p31 ()
  (mapcar
   (lambda (n) (format "%d is prime: %s" n (is-prime n)))
   '(2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
       )))
