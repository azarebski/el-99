

(defun filter-less (x xs)
  (if (null xs)
      nil
    (let ((head (first xs))
          (tail (rest xs)))
      (if (< head x)
          (cons head (filter-less x tail))
        (filter-less x tail))
      )
    )
  )

(defun filter-not-less (x xs)
  (if (null xs)
      nil
    (let ((head (first xs))
          (tail (rest xs)))
      (if (< head x)
          (filter-not-less x tail)
          (cons head (filter-not-less x tail))
        )
      )
    )
  )

(defun quick-sort (xs)
  (if (null xs)
      nil
    (let ((head (first xs))
        (tail (rest xs)))
    (let ((lesser-elems (filter-less head tail))
          (geq-elems (filter-not-less head tail)))
      (append (quick-sort lesser-elems) (list head) (quick-sort geq-elems)))))
)

(defun le-els (x xs)
  (if (null xs)
      nil
    (let ((head (first xs))
          (tail (rest xs)))
      (if (< (length head) (length x))
          (cons head (le-els x tail))
        (le-els x tail)
        )
      )
    )
  )

(defun geq-els (x xs)
  (if (null xs)
      nil
    (let ((head (first xs))
          (tail (rest xs)))
      (if (>= (length head) (length x))
          (cons head (geq-els x tail))
        (geq-els x tail)
        )
      )
    )
  )

;; lsort :: [[a]] -> [[a]]
(defun lsort (xs)
  (if (null xs)
      nil
    (let ((head (first xs))
          (tail (rest xs)))
      (let ((shorter-lists (le-els head tail))
            (not-shorter-lists (geq-els head tail)))
        (append
         (lsort shorter-lists)
         (list head)
         (lsort not-shorter-lists)
         )
        )
      )
    )
  )

