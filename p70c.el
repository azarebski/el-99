(load "~/Documents/el-99/p70b.el")

(defun num-nodes (mt)
  "Return the number of nodes in the multi-tree."
  (if (treep mt)
      (num-nodes-rec mt)
    nil))


(defun num-nodes-rec (mt)
  "Return the number of nodes in the multi-tree."
  (let ((forest (second mt)))
    (if (null-seq forest)
        1
      (+ 1 (-sum (-map 'num-nodes-rec forest))))))
