(load "~/Documents/el-99/p17.el")

(defun fleeb (x)
  (let ((a (car x)) (b (cdr (car (cdr x)))))
    (append a b)))

(defun remove-at (xs n) (fleeb (split xs (- n 1))))
