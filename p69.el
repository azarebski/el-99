(load "parsec")

(defun t-node ()
  (make-symbol (parsec-letter)))

(defun as-nil (x)
  (if (equal x ".") nil))

(defun t-nil ()
  (as-nil (parsec-ch ?.)))

(defun t-tree ()
  (parsec-or (list (t-node) (t-tree) (t-tree)) (t-nil)))
