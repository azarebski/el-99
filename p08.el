
(defun compress-helper (a b) "" ()
       (if (eql a nil)
           b
         (let ((head (car a)) (head-b (car b)) (tail (cdr a)))
           (if (eql head head-b)
               (compress-helper tail b)
             (compress-helper tail (cons head b))))))

(defun compress (l) "" ()
       (reverse (compress-helper l ())))
