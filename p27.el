(load "~/Documents/el-99/p26.el")
(load "dash")

(defun remove-sublist (xs ys)
  (if (null xs)
      ys
    (remove-sublist (cdr xs) (remove (car xs) ys))))


(defun group (ns xs)
  (let ((ys (combination (first ns) xs)))
    (if (= 2 (length ns))
        (mapcar (lambda (y) (list y (remove-sublist y xs))) ys)
      (-flatten-n 1 (mapcar (lambda (y) (mapcar (lambda (z) (cons y z)) (group (rest ns) (remove-sublist y xs)))) ys))
    )
  )
)

