
(defun istree (x)
  (if (null x)
      t
    (if (listp x)
        (if (= 3 (length x))
        (let ((root (first x))
              (left (second x))
              (right (third x)))
          (and (nlistp root) (istree left) (istree right)))
        nil)
      nil)
      )
  )
