
(defun insert-elem (x tree)
  (if (null tree)
      (list x nil nil)
    (let ((root (first tree))
          (left-tree (second tree))
          (right-tree (third tree)))
      (if (<= x root)
          (list root (insert-elem x left-tree) right-tree)
        (list root left-tree (insert-elem x right-tree))))))

(defun bst-helper (tree xs)
  (if (null xs)
      tree
    (bst-helper (insert-elem (first xs) tree) (rest xs))))

(defun bst (xs)
  (bst-helper nil xs))
