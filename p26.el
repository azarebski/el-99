;; (combination 3 '(a b c d e f))

(defun combination (n xs)
  (cond
   ((= n (length xs)) (list xs))
   ((> n 1) (let (
                  (with-first (mapcar (lambda (ys) (cons (first xs) ys)) (combination (- n 1) (rest xs))))
                  (without-first (combination n (rest xs)))
                  )
              (append with-first without-first)
              ))
   (t (mapcar 'list xs))))


;; (combination 1 '(a b c d))
;; (combination 2 '(a b c d))
;; (combination 3 '(a b c d))
;; (combination 4 '(a b c d))
