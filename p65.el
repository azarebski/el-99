
(defun tree-depth (tree)
  (if (and (null (second tree)) (null (third tree)))
      0
    (+ 1 (max (tree-depth (second tree)) (tree-depth (third tree))))))


(defun layout (tree x y depth)
  "Return the node coordinates in the layout."
  (if (null tree)
      nil
    (let ((v (first tree))
          (space (expt 2 (- depth 1))))
      (cons (list v x y) (append (layout (second tree) (- x space) (+ y 1) (- depth 1)) (layout (third tree) (+ x space) (+ y 1) (- depth  1)))))))

(defun layout-2 (tree x y)
  (let ((depth (tree-depth tree)))
    (layout tree x y depth)))

(setq demo-tree '(n (k (c (a nil nil) (e (d nil nil) (g nil nil))) (m nil nil)) (u (p nil (q nil nil)) nil)))

