(load "~/Documents/el-99/p36.el")
(setq max-lisp-eval-depth 10000)
(setq max-specpdl-size 10000)  ; default is 1000, reduce the backtrace level

(defun tp-helper (x)
  (let ((p (first x))
        (m (second x)))
    ;; (expt (* p (- p 1)) (- m 1))))
    (* (- p 1) (expt p (- m 1)))))

(defun sum (x)
  (apply '+ x))

(defun totient-phi-foo (m)
  (let ((prime-mults (prime-factors-mult m)))
    (sum (mapcar 'tp-helper prime-mults))
    ))
