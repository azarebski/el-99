(load "~/Documents/el-99/p03.el")
(load "~/Documents/el-99/p20.el")


;; random-choice :: [a] -> (a, [a])
(defun random-choice (xs)
  (let ((n (length xs)))
    (let ((k (+ 1 (random n))))
      (list (element-at xs k) (remove-at xs k))
      )))

;; rnd-select :: [a] -> Int -> [a]
(defun rnd-select (xs n)
  (if (zerop n)
      nil
    (let ((rnd-choice (random-choice xs)))
        (cons (car rnd-choice) (rnd-select (car (cdr rnd-choice)) (- n 1)))
      )))




;; (rnd-select '(a b c d) 2)

