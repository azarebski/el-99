;; Find the last but one box of a list.


(defun my-but-last (xs)
  (if (<= (length xs) 2)
      xs
    (my-but-last (rest xs))
    )
  )

