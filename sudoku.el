(require 'cl-lib)                       ; provides looping.
(require 'dash)
(require 'seq)                          ; provides array functions.

;; Demonstration board
;; [[1 2] [3 4]] is a complete board
;; [[0 2] [3 4]] is an incomplete board

(defun print-row (row)
  (let ((cell-chars (seq-map 'number-to-string row)))
    (seq-reduce (lambda (a b) (concat (concat a " | ") b)) (cdr cell-chars) (car cell-chars))))

(defun print-board (board)
  "Message the board."
  (let ((row-strings (seq-map 'print-row board)))
    (message (concat " " (seq-reduce (lambda (a b) (concat (concat a "\n-----------\n ") b)) (cdr row-strings) (car row-strings))))
    ))

(defun copy-board (board)
  "Return a deep copy of the board."
  (apply 'vector (seq-map 'copy-sequence board)))

(defun get-cell-value (board coords)
  "Return the element in the jth column of the ith row."
  (let ((i (first coords))
        (j (second coords)))
    (aref (aref board i) j)))

(defun set-cell-value (board i j val)
  "Mutate the element in the jth column of the ith row in the board provided."
  (aset (aref board i) j val))

(defun copy-with-new-value (board coords value)
  "Return a copy of the board with a new value set at the coordinates provided."
  (let ((board-c (copy-board board))
        (i (first coords))
        (j (second coords)))
    (set-cell-value board-c i j value)
    board-c))

(defun transpose-board (board)
  "Returns a transpose of the board."
  (let ((n (length board))
        (result (copy-board board)))
    (dotimes (i n)
      (dotimes (j n)
        (set-cell-value result j i (get-cell-value board (list i j)))))
    result))

(defun row-correct-p (r)
  "Predicate for whether a row is complete and correct."
  (let ((n (length r))
        (min-el (seq-min r))
        (max-el (seq-max r))
        (num-uniq (length (seq-uniq r))))
    (and (= n num-uniq) (= 1 min-el) (= n max-el))))

(defun seq-and (s)
  (seq-reduce (lambda (a b) (and a b)) s t))

(defun board-correct-p (b)
  "Predicate for whether a board is complete and correct."
  (let ((tb (transpose-board b))
        (rows-correct-p (lambda (x) (seq-and (seq-map 'row-correct-p x)))))
    (and (funcall rows-correct-p b) (funcall rows-correct-p tb))))

(defun range-helper (n xs)
  (if (= n 0)
      (cons n xs)
    (range-helper (- n 1) (cons n xs))))

(defun range (n)
  "Return a list 0:(n-1)."
  (range-helper (- n 1) ()))

(defun possible-cell-values (b)
  "Return a list of the possible cell values for a board."
  (-map (lambda (x) (+ x 1)) (range (length b))))

(defun board-coordinates (b)
  "Return a list of the coordinates of the cells in the board."
  (-table-flat 'list (range (length b)) (range (length b))))

(defun empty-cell-p (b coordinate)
  "Predicate for whether a cell of a board is empty."
  (= 0 (get-cell-value b coordinate)))

(defun empty-cell-list (b)
  "Return a list of the coordinates of empty cells."
  (-filter (lambda (c) (empty-cell-p b c)) (board-coordinates b)))

(defun row-possible-p (r)
  "Predicate for whether the row can be completed correctly."
  (let ((pos-cells (seq-filter (lambda (x) (> x 0)) r)))
    (= (length pos-cells) (length (seq-uniq pos-cells)))))

(defun board-possible-p (b)
  "Predicate for whether the board can be completed correctly."
  (let ((rows-possible (seq-and (seq-map 'row-possible-p b)))
        (cols-possible (seq-and (seq-map 'row-possible-p (transpose-board b)))))
    (and rows-possible cols-possible)))

;;-------------------------------------------------------------------------------
;; This is where the main script actual goes.
;;-------------------------------------------------------------------------------

(defun main ()
  (setq my-board (make-vector 9 nil))
  (dotimes (i 9)
    (setf (aref my-board i) (make-vector 9 0)))

  (set-cell-value my-board 0 2 4)
  (set-cell-value my-board 0 3 8)
  (set-cell-value my-board 0 7 1)
  (set-cell-value my-board 0 8 7)

  (set-cell-value my-board 1 0 6)
  (set-cell-value my-board 1 1 7)
  (set-cell-value my-board 1 3 9)
  ;; (set-cell-value my-board 1 4 1)

  (set-cell-value my-board 2 0 5)
  (set-cell-value my-board 2 2 8)
  (set-cell-value my-board 2 4 3)
  (set-cell-value my-board 2 8 4)

  (set-cell-value my-board 3 0 3)
  (set-cell-value my-board 3 3 7)
  (set-cell-value my-board 3 4 4)
  (set-cell-value my-board 3 6 1)

  (set-cell-value my-board 4 1 6)
  (set-cell-value my-board 4 2 9)
  (set-cell-value my-board 4 6 7)
  (set-cell-value my-board 4 7 8)

  (set-cell-value my-board 5 2 1)
  (set-cell-value my-board 5 4 6)
  (set-cell-value my-board 5 5 9)
  (set-cell-value my-board 5 8 5)

  (set-cell-value my-board 6 0 1)
  (set-cell-value my-board 6 4 8)
  (set-cell-value my-board 6 6 3)
  (set-cell-value my-board 6 8 6)

  (set-cell-value my-board 7 5 6)
  (set-cell-value my-board 7 7 9)
  (set-cell-value my-board 7 8 1)

  (set-cell-value my-board 8 0 2)
  (set-cell-value my-board 8 1 4)
  (set-cell-value my-board 8 5 1)
  (set-cell-value my-board 8 6 5)

  my-board
  )

;;-------------------------------------------------------------------------------
;;-------------------------------------------------------------------------------

(defun sudoku (board)
  (fooba board (empty-cell-list board) (possible-cell-values board)))

(defun fooba (board ijs vs)
  (if (not (board-possible-p board))
      nil
    (if (board-correct-p board)
        (list board)
      (let ((attempt (copy-with-new-value board (first ijs) (first vs)))
            (remaining-vs (rest vs)))
        (if (board-possible-p attempt)
            (let ((sol (fooba attempt (rest ijs) (possible-cell-values attempt))))
              (if (null sol)
                  (if (null remaining-vs) nil (fooba board ijs remaining-vs))
                sol))
          (if (null remaining-vs)
              nil
            (fooba board ijs remaining-vs)))))))

(defun foo (board ijs vs)
  (print (format "Number empty cells: %d\nNumber values: %d" (length ijs) (length vs)))
  (print board)
  (print ijs)
  (print vs)
  (if (not (board-possible-p board))
      nil
    (if (board-correct-p board)
          (list board)
      (let ((attempt (copy-with-new-value board (first ijs) (first vs)))
            (remaining-vs (rest vs)))
        (if (board-possible-p attempt)
            (append (foo attempt (rest ijs) (possible-cell-values attempt)) (if (null remaining-vs) nil (foo board ijs remaining-vs)))
          (if (null remaining-vs)
              nil
            (foo board ijs remaining-vs)))))))
